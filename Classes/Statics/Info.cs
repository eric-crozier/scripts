// � 2021, Minetso LLC. All Rights Reserved.

using System;
using System.Diagnostics;

namespace Minetso.Voxscape
{
   public static class Info
   {
      public static Logger Logger;

      #region Methods

      public static string Output(string message)
      {
         string content = $"[ Minetso.Voxcape : LOG ] ";
         content += $"Message : { message  }";
         content = CheckAttribution(content);
         content += "\n\n[ STACK TRACE ]\n";

         if (!(Logger is null))
         {
            Logger.UpdateLog(DateTime.UtcNow.ToString(), message);
         }

         return content;
      }

      private static string CheckAttribution(string content)
      {
         var stackTrace = new StackTrace(true);
         var method = stackTrace.GetFrame(1).GetMethod();

         Attribution[] attributes = stackTrace.GetFrame(1).GetMethod()?.GetCustomAttributes(typeof(Attribution), true) as Attribution[];
         if (attributes is null || attributes.Length < 1) return content;

         content += "\n\n[ METHOD DATA ]\n";
         content += "\n[ Description : " + attributes[0].Description + " ] ";
         content += "\n[ Notes : " + attributes[0].Note + " ] ";
         content += "\n[ Class : " + method.DeclaringType.Name + " ] ";
         content += "\n[ Method : " + method.Name + " ] ";
         content += "\n[ Line : " + stackTrace.GetFrame(1).GetFileLineNumber() + " ] ";

         return content;
      }

      #endregion
   }
}