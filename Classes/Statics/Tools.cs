// � 2021, Minetso LLC. All Rights Reserved.

using System.Collections.Generic;
using UnityEngine;

namespace Minetso.Voxscape
{
   public static class Tools
   {
      #region Methods

      public static Vector3Int V3FTOV3I(Vector3 v)
      {
         return new Vector3Int(Mathf.FloorToInt(v.x), Mathf.FloorToInt(v.y), Mathf.FloorToInt(v.z));
      }
      public static V4Int V4FTOV4F(Vector4 v)
      {
         return new V4Int(Mathf.FloorToInt(v.x), Mathf.FloorToInt(v.y), Mathf.FloorToInt(v.z), Mathf.FloorToInt(v.w));
      }

      public struct V4Int
      {
         public int X;
         public int Y;
         public int Z;
         public int W;

         public V4Int(int x, int y, int z, int w)
         {
            X = x;
            Y = y;
            Z = z;
            W = w;
         }
      }


      public static void UpdateChildTags(Transform t, string tag)
      {
         t.gameObject.tag = tag;

         foreach (Transform child in t)
         {
            UpdateChildTags(child, tag);
         }
      }

      public static Vector3Int ROTV(Vector3 point, float y)
      {
         var angle = -y * (Mathf.PI / 180f);
         int rotatedX = (int)(Mathf.Cos(angle) * (point.x - 0) - Mathf.Sin(angle) * (point.z - 0) + 0);
         int rotatedZ = (int)(Mathf.Sin(angle) * (point.x - 0) + Mathf.Cos(angle) * (point.z - 0) + 0);

         return new Vector3Int(rotatedX, (int)point.y, rotatedZ);
      }

      public static RaycastHit FireRay(Vector3 pos, Vector3 direction)
      {
         Physics.Raycast(new Ray { origin = pos, direction = direction }, out RaycastHit hit);
         return hit;
      }

      public static Vector3 V3ITOV3F(Vector3Int v)
      {
         return new Vector3(v.x, v.y, v.z);
      }
      public static List<Vector3> V3ILTOV3FL(List<Vector3Int> v)
      {
         List<Vector3> newList = new List<Vector3>();
         foreach (var vector in v)
         {
            newList.Add(V3ITOV3F(vector));
         }

         return newList;
      }

      #endregion
   }
}