// � 2021, Minetso LLC. All Rights Reserved.

using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.VFX;

namespace Minetso.Voxscape
{
   public static class ParticleController
   {
      public static void SetParticles(Dictionary<Vector3Int, PointData> points, VisualEffect tempVfx)
      {
         int totalKeys = points.Keys.Count;
         var size = Mathf.CeilToInt(Mathf.Sqrt(totalKeys * 6));
         var texPrimaryColor = new Texture2D(size, size, TextureFormat.RGBAFloat, false);
         var texSecondaryColor = new Texture2D(size, size, TextureFormat.RGBAFloat, false);
         var mainTexture = new Texture2D(size, size, TextureFormat.RGBAFloat, false);
         var texPosScale = new Texture2D(size, size, TextureFormat.RGBAFloat, false);
         var texRotScale = new Texture2D(size, size, TextureFormat.RGBAFloat, false);
         int count = 0;
         var keys = points.Keys.ToList();
         int totalPoints = 0;

         int Check(Vector3Int direction, int x, int count, Vector3 position, Vector3Int rotation, int faceIndex, int oppositeFaceIndex)
         {
            var mainPoint = points[keys[count]];
            var y = Mathf.FloorToInt(x / size);
            Color offset = new Color(position.x, position.y, position.z);
            Vector3 pos = new Vector3(keys[count].x + offset.r, keys[count].y + offset.g, keys[count].z + offset.b);
            Vector3Int neighborPos = Tools.V3FTOV3I(keys[count] + direction);


            if (mainPoint.Faces[faceIndex].Emissive) mainPoint.Faces[faceIndex].Transparent = false;
            if (mainPoint.Lit)
            {
               mainPoint.Faces[faceIndex].Emissive = true;
               mainPoint.Faces[faceIndex].Transparent = true;
            }
               if (!points.ContainsKey(neighborPos) || (points.ContainsKey(neighborPos) && mainPoint.Faces[faceIndex].Transparent == false && (points[neighborPos].Faces[oppositeFaceIndex].Transparent == true || points[neighborPos].Lit == true)))
            {
               var newColor = mainPoint.Faces[faceIndex].BlockPrimaryColor;

               var finalColor = new Color(newColor.r, newColor.g, newColor.b, mainPoint.Faces[faceIndex].Transparent ? .1f : 1f);
               texPrimaryColor.SetPixel(x % size, y, finalColor);
               texSecondaryColor.SetPixel(x % size, y, mainPoint.Faces[faceIndex].BlockSecondaryColor);
               mainTexture.SetPixel(x % size, y, new Color(mainPoint.Faces[faceIndex].TextureIndex.x, mainPoint.Faces[faceIndex].TextureIndex.y, 0, 0));
               texPosScale.SetPixel(x % size, y, new Color(pos.x, pos.y, pos.z));
               texRotScale.SetPixel(x % size, y, new Color(rotation.x, rotation.y, rotation.z, mainPoint.Faces[faceIndex].Emissive ? 1f : 0f ));

               if (mainPoint.Lit && mainPoint.Light == null) BlockDesigner.SpawnLight(keys[count], finalColor, points);

               x++;
               totalPoints++;
            }
            return x;
         }

         for (int x = 0; x < size * size;)
         {
            if (count >= keys.Count)
            {
               goto here;
            }

            x = Check(Vector3Int.up, x, count, new Vector3(0, .5f, 0), new Vector3Int(0, 180 + points[keys[count]].Faces[0].TextureIndex.z, 0), 0, 1);
            x = Check(Vector3Int.down, x, count, new Vector3(0, -.5f, 0), new Vector3Int(180, 180 - points[keys[count]].Faces[1].TextureIndex.z, 0), 1, 0);

            x = Check(Vector3Int.back, x, count, new Vector3(0, 0, -.5f), new Vector3Int(90 + points[keys[count]].Faces[2].TextureIndex.z, 90, 270), 2, 3); // Forward
            x = Check(Vector3Int.forward, x, count, new Vector3(0, 0, .5f), new Vector3Int(90 + points[keys[count]].Faces[3].TextureIndex.z, -90, 270), 3, 2); // Back

            x = Check(Vector3Int.right, x, count, new Vector3(.5f, 0, 0), new Vector3Int(90 + points[keys[count]].Faces[4].TextureIndex.z, 0, -90), 4, 5);
            x = Check(Vector3Int.left, x, count, new Vector3(-.5f, 0, 0), new Vector3Int(90 - points[keys[count]].Faces[5].TextureIndex.z, 0, 90), 5, 4);

            count++;
         }
      here:;
         texPrimaryColor.Apply();
         texSecondaryColor.Apply();
         mainTexture.Apply();
         texPosScale.Apply();
         texRotScale.Apply();
         tempVfx.Reinit();
         tempVfx.SetUInt(Shader.PropertyToID("Particle Count"), (uint)totalPoints);
         tempVfx.SetTexture(Shader.PropertyToID("Primary Color"), texPrimaryColor);
         tempVfx.SetTexture(Shader.PropertyToID("Secondary Color"), texSecondaryColor);
         tempVfx.SetTexture(Shader.PropertyToID("Texture Index"), mainTexture);
         tempVfx.SetTexture(Shader.PropertyToID("Position"), texPosScale);
         tempVfx.SetTexture(Shader.PropertyToID("Rotation"), texRotScale);
         tempVfx.SetUInt(Shader.PropertyToID("Scale"), (uint)size);
         tempVfx.SendEvent("Start Vfx Event");
      }
   }
}