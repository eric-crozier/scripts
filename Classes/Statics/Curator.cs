// � 2021, Minetso LLC. All Rights Reserved.

using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using UnityEngine;
using UnityEngine.XR;

namespace Minetso.Voxscape
{
   public static class Curator
   {
      public static void LoadThemes()
      {
         List<Theme> themes = new List<Theme>();

         string root = $"{Application.persistentDataPath}/Themes";

         if (!Directory.Exists(root))
         {
            Debug.Log(Info.Output($"Theme directory missing at path ( { root } ). Creating a new directory."));
            Directory.CreateDirectory(root);
            return;
         }
         else
         {
            var files = Directory.GetFiles(root);

            foreach (var file in files)
            {
               Debug.Log(Info.Output($"Saved themes path is : {file}"));
               themes.Add(JsonUtility.FromJson(File.ReadAllText(file), typeof(Theme)) as Theme);
            }
         }

         WorldData.LoadedThemes = themes;
      }



      public static void SaveTheme(Theme theme)
      {
         string root = $"{Application.persistentDataPath}/Themes";

         if (!Directory.Exists(root))
         {
            Debug.Log(Info.Output($"Theme directory missing at path ( { root } ). Creating a new directory."));
            Directory.CreateDirectory(root);
         }
         string path = $"{ root}/{theme.Name}.vt";

         if (File.Exists(path))
         {
            File.Move(path, $"{path}.temp");
         }

         File.WriteAllText(path, JsonUtility.ToJson(theme));

         if (File.Exists($"{path}.temp"))
         {
            File.Delete($"{path}.temp");
         }

         LoadThemes();
      }

   }
}