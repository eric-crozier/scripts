// � 2021, Minetso LLC. All Rights Reserved.

using System;

namespace Minetso.Voxscape
{
   [Attribution(Description = "Attribution class", Note = "Adds information about a field, property, class or structure.")]
   public sealed class Attribution : Attribute
   {
      public string Description;
      public string Note;
   }
}