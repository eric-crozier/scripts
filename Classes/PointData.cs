// � 2021, Minetso LLC. All Rights Reserved.

using System.Collections.Generic;
using UnityEngine;

namespace Minetso.Voxscape
{
   [System.Serializable]
   public sealed class PointData
   {
      public Vector3Int Position;
      public bool Lit;
      public GameObject Light;

      public List<Face> Faces = new List<Face>();

      [System.Serializable]
      public class Face
      {
         public Color BlockPrimaryColor;
         public Color BlockSecondaryColor;
         public Vector3Int TextureIndex;
         public bool Transparent;
         public bool Emissive;
      }
   }
}