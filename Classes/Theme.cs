// � 2021, Minetso LLC. All Rights Reserved.

using System.Collections.Generic;

namespace Minetso.Voxscape
{
   [System.Serializable]
   public sealed class Theme
   {
      public string Name;
      public List<TagData> Tags = new List<TagData>();

      [System.Serializable]
      public sealed class TagData
      {
         public string MainTag;
         public List<SubTagData> SubTags = new List<SubTagData>();

         [System.Serializable]
         public sealed class SubTagData
         {
            public string SubTag;
            public List<BlockData> Blocks = new List<BlockData>();
         }
      }
   }
}