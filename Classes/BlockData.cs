// � 2021, Minetso LLC. All Rights Reserved.

using Sirenix.OdinInspector;
using System.Collections.Generic;
using UnityEngine;

namespace Minetso.Voxscape
{
   [HideMonoScript] [System.Serializable]
   public class BlockData
   {
      #region Properties / Fields

      public string Name;
      public string MainTag;
      public string SubTag;
      public bool[] Connections = new bool[6];
      public Vector3Int Position;
      public Vector3Int Rotation;

      [ShowInInspector] [SerializeField] public Dictionary<Vector3Int, PointData> points = new Dictionary<Vector3Int, PointData>();
      public List<PointData> pointData = new List<PointData>();

      public BlockData(BlockData blockData)
      {
         Name = blockData.Name;
         MainTag = blockData.MainTag;
         SubTag = blockData.SubTag;
         Connections = new bool[6];
         Position = blockData.Position;
         Rotation = blockData.Rotation;

         points.Clear();
         foreach (var point in blockData.pointData)
         {
            points.Add(point.Position, point);
         }
      }

      public BlockData() { }

      #endregion
   }
}