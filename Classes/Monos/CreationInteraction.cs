// � 2021, Minetso LLC. All Rights Reserved.

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;
using System.Linq;

namespace Minetso.Voxscape
{
   [ExecuteInEditMode]
   public class CreationInteraction : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
   {
      [SerializeField] private Canvas Canvas;
      [SerializeField] private Button ColorButton;
      [SerializeField] private Button PrimaryButton;
      [SerializeField] private Button SecondaryButton;
      [SerializeField] private Button TextureButton;
      [SerializeField] private Button TextureSideInternactionButton;
      [SerializeField] private Image TextureSideInternactionImage;
      [SerializeField] private Image TextureImage;

      [SerializeField] private Slider ColorBarSizeSlider;
      [SerializeField] private GameObject Selector;
      [SerializeField] private Vector2Int ColorSize = new Vector2Int(256, 16);
      [SerializeField] private Vector2Int PrimaryIndex;
      [SerializeField] private Vector2Int SecondaryIndex;
      [SerializeField] private Vector3Int TextureIndex = new Vector3Int(0, 0, 0);
      [SerializeField] private Texture2D mainTexture;

      private RectTransform buttonRect;
      private bool isOver = false;
      private bool isPrimary = true;
      private float timer;
      [SerializeField] private float delay = .1f;
      public static CreationInteraction CreationInteractor;
      [SerializeField]
      private static Dictionary<Vector3Int, Vector2Int> colorMap = new Dictionary<Vector3Int, Vector2Int>();

      public TMP_InputField ThemeInputField;
      public TMP_Dropdown ThemeDropdown;
      public TMP_InputField BlockNameInputField;
      public TMP_Dropdown BlockNameDropdown;
      public TMP_InputField BlockMainTagInputField;
      public TMP_Dropdown BlockMainTagDropdown;
      public TMP_InputField BlockSubTagInputField;
      public TMP_Dropdown BlockSubTagDropdown;

      public static void RefreshColors(Color primaryColor, Color secondaryColor, Vector3Int textureIndex)
      {
         Vector3Int primary = new Vector3Int(Mathf.FloorToInt(primaryColor.r * 1000f), Mathf.FloorToInt(primaryColor.g * 1000f), Mathf.FloorToInt(primaryColor.b * 1000f));
         Vector3Int secondary = new Vector3Int(Mathf.FloorToInt(secondaryColor.r * 1000f), Mathf.FloorToInt(secondaryColor.g * 1000f), Mathf.FloorToInt(secondaryColor.b * 1000f));
         Debug.Log(Info.Output($"primary = [{primary}]"));
         Debug.Log(Info.Output($"secondary = [{secondary}]"));
         Debug.Log(Info.Output($"primaryColor = [{primaryColor}]"));
         Debug.Log(Info.Output($"secondaryColor = [{secondaryColor}]"));
         Debug.Log(Info.Output($"ColorBuilder.PrimaryIndex = [{CreationInteractor.PrimaryIndex}]"));
         CreationInteractor.PrimaryIndex = colorMap.ContainsKey(primary) ? colorMap[primary] : new Vector2Int(0, 0);
         Debug.Log(Info.Output($"ColorBuilder.PrimaryIndex = [{CreationInteractor.PrimaryIndex}]"));
         CreationInteractor.SecondaryIndex = colorMap.ContainsKey(secondary) ? colorMap[secondary] : new Vector2Int(0, 0);
         CreationInteractor.TextureIndex = textureIndex;
         CreationInteractor.isPrimary = false;
         CreationInteractor.RoundIndex(CreationInteractor.SecondaryIndex);
         CreationInteractor.isPrimary = true;
         CreationInteractor.RoundIndex(CreationInteractor.PrimaryIndex);

         CreationInteractor.SetTextureImage(false);
         CreationInteractor.TextureSideInternactionImage.transform.eulerAngles = new Vector3(0, 0, -textureIndex.z);



      }

      void Start()
      {
         CreationInteractor = this;
         ColorButton.onClick.AddListener(OnMouseClickOnButton);
         buttonRect = (ColorButton.transform as RectTransform);
         ColorBarSizeSlider.onValueChanged.AddListener(delegate { BuildColorBar(); });
         PrimaryButton.onClick.AddListener(delegate { isPrimary = true; RoundIndex(PrimaryIndex); });
         SecondaryButton.onClick.AddListener(delegate { isPrimary = false; RoundIndex(SecondaryIndex); });
         TextureButton.onClick.AddListener(delegate { SetTextureImage(true); });
         BuildColorBar();
         isPrimary = false;
         RoundIndex(SecondaryIndex);
         isPrimary = true;
         RoundIndex(PrimaryIndex);


         BlockDesigner.blockDesigner.BlockPrimaryColor = GetColorWithIndex(PrimaryIndex);
         BlockDesigner.blockDesigner.BlockSecondaryColor = GetColorWithIndex(SecondaryIndex);
         BlockDesigner.blockDesigner.TextureIndex = TextureIndex;

         Curator.LoadThemes();
         UpdateThemeDropdown();
      }

      public void SaveBlock()
      {
         BlockDesigner.blockDesigner.blockData.MainTag = BlockMainTagDropdown.captionText.text;
         BlockDesigner.blockDesigner.blockData.SubTag = BlockSubTagDropdown.captionText.text;
         BlockDesigner.blockDesigner.blockData.Name = BlockNameDropdown.captionText.text;
         Debug.Log(Info.Output($"Block name is : {BlockNameDropdown.captionText.text}"));
         BlockDesigner.blockDesigner.blockData.pointData = BlockDesigner.blockDesigner.blockData.points.Values.ToList();
         var blocks = WorldData.LoadedThemes[ThemeDropdown.value].Tags[BlockMainTagDropdown.value].SubTags[BlockSubTagDropdown.value].Blocks;
         try
         {
            for (int i = 0; i < blocks.Count; i++)
            {
               if (blocks[i].Name.ToUpper() == BlockNameDropdown.captionText.text.ToUpper())
               {
                  blocks[i] = BlockDesigner.blockDesigner.blockData;
                  Debug.Log(Info.Output($"Saving block."));
                  Curator.SaveTheme(WorldData.LoadedThemes[ThemeDropdown.value]);
                  return;
               }
            }
            Debug.Log(Info.Output($"Adding new block, and saving."));
            WorldData.LoadedThemes[ThemeDropdown.value].Tags[BlockMainTagDropdown.value].SubTags[BlockSubTagDropdown.value].Blocks.Add(BlockDesigner.blockDesigner.blockData);
            Curator.SaveTheme(WorldData.LoadedThemes[ThemeDropdown.value]);
         }
         catch
         {

         }
      }

      public void LoadBlock()
      {
         Curator.LoadThemes();
         Debug.Log(Info.Output($"Loading blocks."));
         var blocks = WorldData.LoadedThemes[ThemeDropdown.value].Tags[BlockMainTagDropdown.value].SubTags[BlockSubTagDropdown.value].Blocks;
         Debug.Log(Info.Output($"Loading block name is : {BlockNameDropdown.captionText.text}"));
         Debug.Log(Info.Output($"Found {blocks.Count} blocks."));
         if (blocks == null) return;

         foreach (var block in blocks)
         {
            if (block.Name.ToUpper() == BlockNameDropdown.captionText.text.ToUpper())
            {
               BlockDesigner.blockDesigner.blockData = block;
               foreach (var point in block.pointData)
               {
                  block.points.Add(point.Position, point);
               }
               ParticleController.SetParticles(block.points, BlockDesigner.TempVfx);
               Debug.Log(Info.Output($"Setting blocks."));
               return;
            }
         }
      }

      public void NewBlock()
      {
         BlockDesigner.blockDesigner.blockData = new BlockData();
         ParticleController.SetParticles(BlockDesigner.blockDesigner.blockData.points, BlockDesigner.TempVfx);
      }

      public void ChangeConnection(Toggle toggle)
      {
         BlockDesigner.blockDesigner.blockData.Connections[toggle.transform.GetSiblingIndex()] = toggle.isOn;
      }

      public void ChangeMainTag()
      {
         if (WorldData.LoadedThemes[ThemeDropdown.value].Tags.Count <= BlockMainTagDropdown.value) return;
         BlockDesigner.blockDesigner.blockData.MainTag = WorldData.LoadedThemes[ThemeDropdown.value].Tags[BlockMainTagDropdown.value].MainTag;
      }

      public void ChangeSubTag()
      {
         if (WorldData.LoadedThemes[ThemeDropdown.value].Tags[BlockMainTagDropdown.value].SubTags.Count <= BlockSubTagDropdown.value) return;
         BlockDesigner.blockDesigner.blockData.SubTag = WorldData.LoadedThemes[ThemeDropdown.value].Tags[BlockMainTagDropdown.value].SubTags[BlockSubTagDropdown.value].SubTag;
      }
      /*
      public void ChangeName()
      {
         BlockDesigner.blockDesigner.blockData.Name = BlockNameDropdown.captionText.text;
      }
      */
      public void AddTheme(TMP_InputField themeName)
      {
        // Curator.SaveTheme(new Theme() { Name = themeName.text });
         UpdateThemeDropdown();
      }
      public void AddMainTag(TMP_InputField mainTagName)
      {
         var tags = WorldData.LoadedThemes[ThemeDropdown.value].Tags;

         for (int i = 0; i < tags.Count; i++)
         {
            if (tags[i].MainTag.ToUpper() == mainTagName.text.ToUpper())
            {
               return;
            }
         }
         tags.Add(new Theme.TagData() { MainTag = mainTagName.text });
         //UpdateMainTagDropdown();
         var newOption = new TMP_Dropdown.OptionData() { text = mainTagName.text };
         BlockMainTagDropdown.options.Add(newOption);
         for (int i = 0; i < BlockMainTagDropdown.options.Count; i++)
         {
            if (BlockMainTagDropdown.options[i] == newOption)
            {
               BlockMainTagDropdown.value = i;
               break;
            }
         }
         BlockMainTagDropdown.RefreshShownValue();
         BlockSubTagDropdown.options.Clear();
         //Curator.SaveTheme(WorldData.LoadedThemes[ThemeDropdown.value]);
      }

      public void AddSubTag(TMP_InputField subTagName)
      {
         var tags = WorldData.LoadedThemes[ThemeDropdown.value].Tags[BlockMainTagDropdown.value].SubTags;

         for (int i = 0; i < tags.Count; i++)
         {
            if (tags[i].SubTag.ToUpper() == subTagName.text.ToUpper())
            {
               return;
            }
         }
         var newOption = new TMP_Dropdown.OptionData() { text = subTagName.text };
         tags.Add(new Theme.TagData.SubTagData() { SubTag = subTagName.text });

         BlockSubTagDropdown.options.Add(newOption);
         for (int i = 0; i < BlockSubTagDropdown.options.Count; i++)
         {
            if (BlockSubTagDropdown.options[i] == newOption)
            {
               BlockSubTagDropdown.value = i;
               break;
            }
         }
         BlockSubTagDropdown.RefreshShownValue();
         //UpdateSubTagDropdown();

         //Curator.SaveTheme(WorldData.LoadedThemes[ThemeDropdown.value]);
      }

      public void AddName(TMP_InputField newName)
      {
         BlockNameDropdown.ClearOptions();
         if (WorldData.LoadedThemes[ThemeDropdown.value].Tags[BlockMainTagDropdown.value].SubTags.Count <= BlockSubTagDropdown.value) return;
         foreach (var block in WorldData.LoadedThemes[ThemeDropdown.value].Tags[BlockMainTagDropdown.value].SubTags[BlockSubTagDropdown.value].Blocks)
         {
            BlockNameDropdown.options.Add(new TMP_Dropdown.OptionData() { text = block.Name });
         }
         var newOption = new TMP_Dropdown.OptionData() { text = newName.text };
         BlockNameDropdown.options.Add(newOption);

         for (int i = 0; i < BlockNameDropdown.options.Count; i++)
         {
            if (BlockNameDropdown.options[i] == newOption)
            {
               BlockNameDropdown.value = i;
               break;
            }
         }

         BlockNameDropdown.RefreshShownValue();

         /*
         var blocks = WorldData.LoadedThemes[ThemeDropdown.value].Tags[BlockMainTagDropdown.value].SubTags[BlockSubTagDropdown.value].Blocks;
         for (int i = 0; i < blocks.Count; i++)
         {
            if (blocks[i].Name.ToUpper() == newName.text.ToUpper())
            {
               UpdateNameDropdown();
               Curator.SaveTheme(WorldData.LoadedThemes[ThemeDropdown.value]);
               return;
            }
         }
         BlockDesigner.blockDesigner.blockData.Name = newName.text;
         blocks.Add(BlockDesigner.blockDesigner.blockData);
         UpdateNameDropdown();
         Curator.SaveTheme(WorldData.LoadedThemes[ThemeDropdown.value]);
         */
      }

      private void UpdateThemeDropdown()
      {
         ThemeDropdown.ClearOptions();
         foreach (var theme in WorldData.LoadedThemes)
         {
            ThemeDropdown.options.Add(new TMP_Dropdown.OptionData() { text = theme.Name });
         }
         ThemeDropdown.RefreshShownValue();
         UpdateMainTagDropdown();
      }

      public void OnThemeDropdownChange()
      {
         UpdateMainTagDropdown();
      }

      public void OnMainTagDropdownChange()
      {
         UpdateSubTagDropdown();
      }

      private void UpdateMainTagDropdown()
      {
         BlockMainTagDropdown.ClearOptions();
         if (WorldData.LoadedThemes.Count <= ThemeDropdown.value) return;
         foreach (var tag in WorldData.LoadedThemes[ThemeDropdown.value].Tags)
         {
            BlockMainTagDropdown.options.Add(new TMP_Dropdown.OptionData() { text = tag.MainTag });
         }
         BlockMainTagDropdown.RefreshShownValue();
         UpdateSubTagDropdown();
      }

      private void UpdateSubTagDropdown()
      {
         BlockSubTagDropdown.ClearOptions();
         if (WorldData.LoadedThemes[ThemeDropdown.value].Tags.Count <= BlockMainTagDropdown.value) return;
         foreach (var tag in WorldData.LoadedThemes[ThemeDropdown.value].Tags[BlockMainTagDropdown.value].SubTags)
         {
            BlockSubTagDropdown.options.Add(new TMP_Dropdown.OptionData() { text = tag.SubTag});
         }
         BlockSubTagDropdown.RefreshShownValue();
         UpdateNameDropdown();
      }

      private void UpdateNameDropdown()
      {
         BlockNameDropdown.ClearOptions();
         if (WorldData.LoadedThemes[ThemeDropdown.value].Tags[BlockMainTagDropdown.value].SubTags.Count <= BlockSubTagDropdown.value) return;
         foreach (var block in WorldData.LoadedThemes[ThemeDropdown.value].Tags[BlockMainTagDropdown.value].SubTags[BlockSubTagDropdown.value].Blocks)
         {
            BlockNameDropdown.options.Add(new TMP_Dropdown.OptionData() { text = block.Name });
         }
         BlockNameDropdown.RefreshShownValue();
      }

      public void UpdateTextureColors()
      {
         //TextureBackgroundImage.color = GetColorWithIndex(PrimaryIndex);

         var texture = TextureImage.sprite.texture;
         for (int x = 0; x < texture.width; x++)
         {
            for (int y = 0; y < texture.height; y++)
            {
               var pixel = mainTexture.GetPixel(x, y);
               texture.SetPixel(x, y, pixel);
               /*
               if (pixel == Color.black)
               {
                  texture.SetPixel(x, y, GetColorWithIndex(SecondaryIndex));
               }
               else if (pixel == Color.grey)
               {
                  texture.SetPixel(x, y, GetColorWithIndex(PrimaryIndex) * .5f);
               }
               else
               {
                  texture.SetPixel(x, y, GetColorWithIndex(PrimaryIndex));
               }
               */
            }
         }
         texture.Apply();

      }

      private void SetTextureImage(bool updateLocation)
      {
         Texture2D newTexture = new Texture2D(16, 16);
         if (updateLocation)
         {
            var mousePos = Input.mousePosition;
            var rect = (TextureButton.transform as RectTransform).rect;
            var width = rect.width * Canvas.scaleFactor / 64;
            var height = rect.height * Canvas.scaleFactor / 4;
            var center = TextureButton.transform.position;

            TextureIndex = new Vector3Int(
              Mathf.FloorToInt((mousePos.x - (center.x - ((rect.width * Canvas.scaleFactor) * .5f))) / width),
               Mathf.FloorToInt((mousePos.y - (center.y - ((rect.height * Canvas.scaleFactor) * .5f))) / height),
               TextureIndex.z
               );
         }
         TextureIndex.x %= 64;
         TextureIndex.y %= 4;

         if (TextureIndex.x < 0)
         {
            TextureIndex.x = 63;
         }
         if (TextureIndex.y < 0)
         {
            TextureIndex.y = 3;
         }


         for (int x = 0; x < newTexture.width; x++)
         {
            for (int y = 0; y < newTexture.height; y++)
            {
               var pixel = mainTexture.GetPixel(x + (TextureIndex.x * 16), y + (TextureIndex.y * 16));
               //newTexture.SetPixel(x, y, pixel);

               if (pixel == Color.black)
               {
                  newTexture.SetPixel(x, y, GetColorWithIndex(SecondaryIndex));
               }
               else
               {
                  newTexture.SetPixel(x, y, GetColorWithIndex(PrimaryIndex) * ((pixel.r + pixel.g + pixel.b) / 3f));
               }

            }
         }
         newTexture.filterMode = FilterMode.Point;
         newTexture.Apply();
         TextureSideInternactionButton.image.color = GetColorWithIndex(SecondaryIndex);
         TextureSideInternactionImage.sprite = Sprite.Create(newTexture, new Rect(0, 0, newTexture.width, newTexture.height), new Vector2(0, 0));

         BlockDesigner.blockDesigner.TextureIndex = TextureIndex;
      }

      public void Update()
      {
         timer += Time.deltaTime;
         if (timer > delay)
         {
            if (Input.GetKey(Controls.RotateTexture))
            {
               TextureIndex = new Vector3Int(TextureIndex.x, TextureIndex.y, (TextureIndex.z + 90) % 360);
               TextureSideInternactionImage.transform.eulerAngles = new Vector3(0, 0, -TextureIndex.z);
               SetTextureImage(false);
               timer = 0;
            }

            if (Input.GetKey(Controls.UpArrow))
            {
               if (TextureButton.gameObject.activeSelf)
               {
                  TextureIndex.y++;
                  SetTextureImage(false);
               }
               else
               {
                  if (isPrimary)
                  {
                     PrimaryIndex.y++;
                     RoundIndex(PrimaryIndex);
                  }
                  else
                  {
                     SecondaryIndex.y++;
                     RoundIndex(SecondaryIndex);
                  }
               }

               timer = 0;
            }
            if (Input.GetKey(Controls.DownArrow))
            {
               if (TextureButton.gameObject.activeSelf)
               {
                  TextureIndex.y--;
                  SetTextureImage(false);
               }
               else
               {
                  if (isPrimary)
                  {
                     PrimaryIndex.y--;
                     RoundIndex(PrimaryIndex);
                  }
                  else
                  {
                     SecondaryIndex.y--;
                     RoundIndex(SecondaryIndex);
                  }
               }
               timer = 0;
            }
            if (Input.GetKey(Controls.LeftArrow))
            {
               if (TextureButton.gameObject.activeSelf)
               {
                  TextureIndex.x--;
                  SetTextureImage(false);
               }
               else
               {
                  if (isPrimary)
                  {
                     PrimaryIndex.x--;
                     RoundIndex(PrimaryIndex);
                  }
                  else
                  {
                     SecondaryIndex.x--;
                     RoundIndex(SecondaryIndex);
                  }
               }
               timer = 0;
            }
            if (Input.GetKey(Controls.RightArrow))
            {
               if (TextureButton.gameObject.activeSelf)
               {
                  TextureIndex.x++;
                  SetTextureImage(false);
               }
               else
               {
                  if (isPrimary)
                  {
                     PrimaryIndex.x++;
                     RoundIndex(PrimaryIndex);
                  }
                  else
                  {
                     SecondaryIndex.x++;
                     RoundIndex(SecondaryIndex);
                  }
               }
               timer = 0;
            }
         }


         if (isOver)
         {
            var pos = Input.mousePosition;
            var buttonPos = ColorButton.transform.position;
            var buttonWidth = (ColorButton.transform as RectTransform).rect.width;
            var buttonHeight = (ColorButton.transform as RectTransform).rect.height;
            var xMax = buttonPos.x + (buttonWidth);
            var yMax = buttonPos.y + (buttonHeight);
            var xMin = buttonPos.x - (buttonWidth);
            var yMin = buttonPos.y - (buttonHeight);

            if (pos.x > xMax)
            {
               pos.x = xMax;
            }
            else if (pos.x < xMin)
            {
               pos.x = xMin;
            }

            if (pos.y > yMax)
            {
               pos.y = yMax;
            }
            else if (pos.y < yMin)
            {
               pos.y = yMin;
            }
         }
      }

      private void SetIndex(Vector2 targetValue)
      {
         var width = ((buttonRect.rect.width * Canvas.scaleFactor) / ColorSize.x);
         var height = ((buttonRect.rect.height * Canvas.scaleFactor) / ColorSize.y);
         var center = buttonRect.transform.position;

         var newValue = new Vector2Int(
           Mathf.FloorToInt((targetValue.x - (center.x - ((buttonRect.rect.width * Canvas.scaleFactor) * .5f))) / width),
            Mathf.FloorToInt((targetValue.y - (center.y - ((buttonRect.rect.height * Canvas.scaleFactor) * .5f))) / height)
            );

         if (isPrimary)
         {
            PrimaryIndex = newValue;
         }
         else
         {
            SecondaryIndex = newValue;
         }

         RoundIndex(isPrimary ? PrimaryIndex : SecondaryIndex);
      }

      private void RoundIndex(Vector2Int index)
      {
         var width = ((buttonRect.rect.width * Canvas.scaleFactor) / ColorSize.x);
         var height = ((buttonRect.rect.height * Canvas.scaleFactor) / ColorSize.y);
         var center = buttonRect.transform.position;
         index.x %= ColorSize.x;
         index.y %= ColorSize.y;

         if (index.x < 0)
         {
            index.x = Mathf.FloorToInt(ColorSize.x + index.x);
         }
         if (index.y < 0)
         {
            index.y = Mathf.FloorToInt(ColorSize.y + index.y);
         }
         //Debug.Log(Info.Output($"center = {center} Width = {buttonRect.rect.width} Height = {buttonRect.rect.height}"));
         Selector.transform.position = (new Vector3(
            (center.x - ((buttonRect.rect.width * Canvas.scaleFactor) * .5f)) + (index.x * width) + (width * .5f),
            (center.y - ((buttonRect.rect.height * Canvas.scaleFactor) * .5f)) + (index.y * height) + (height * .5f),
            0));

         (Selector.transform as RectTransform).sizeDelta = new Vector2(width, height) * Canvas.scaleFactor * .5f;
         if (isPrimary)
         {
            PrimaryButton.image.color = GetColorWithIndex(index);
         }
         else
         {
            SecondaryButton.image.color = GetColorWithIndex(index);
         }
         SetTextureImage(false);

         BlockDesigner.blockDesigner.BlockPrimaryColor = GetColorWithIndex(PrimaryIndex);
         BlockDesigner.blockDesigner.BlockSecondaryColor = GetColorWithIndex(SecondaryIndex);
         BlockDesigner.blockDesigner.TextureIndex = TextureIndex;
         // Debug.Log(Info.Output($"Selector.transform.position = {Selector.transform.position}"));
      }

      private Color GetColorWithIndex(Vector2Int index)
      {
         return ColorButton.image.sprite.texture.GetPixel(index.x, index.y);
      }

      private void OnMouseClickOnButton()
      {
         var pos = Input.mousePosition;
         //Debug.Log(Info.Output($"rect.rect.width = {buttonRect.rect.width}  rect.rect.height = { buttonRect.rect.height} "));
         SetIndex(pos);

      }

      public void OnPointerEnter(PointerEventData eventData)
      {
         isOver = true;
      }

      public void OnPointerExit(PointerEventData eventData)
      {
         isOver = false;
      }

      public void BuildColorBar()
      {
         colorMap = new Dictionary<Vector3Int, Vector2Int>();
         ColorSize.x = (int)(((ColorBarSizeSlider.value + 1) * 128) * .5f);
         ColorSize.y = (int)(((ColorBarSizeSlider.value + 1) * 8) * .5f);

         var width = ColorSize.x;
         var height = ColorSize.y;
         Texture2D colorGradient = new Texture2D(width, height);

         for (int h = 0; h < height; h++)
         {
            var newH = (height - 1) - h;
            float offset = 1f - ((float)h / (height - 1f));
            var color = new Color(offset, offset, offset, 1);
            colorGradient.SetPixel(0, newH, color);
            var pixel = colorGradient.GetPixel(0, newH);
            var rounded = new Vector3Int(Mathf.FloorToInt(pixel.r * 1000f), Mathf.FloorToInt(pixel.g * 1000f), Mathf.FloorToInt(pixel.b * 1000f));
            if (!colorMap.ContainsKey(rounded)) colorMap.Add(rounded, new Vector2Int(0, newH));
         }

         for (int w = 1; w < width; w++)
         {
            for (int h = 0; h < height / 2; h++)
            {
               var newH = (height - 1) - h;
               var color = Color.HSVToRGB((float)(w + 1) / (width - 1), (float)((h + 1) * 2f) / (height - 1), 1);
               colorGradient.SetPixel(w, newH, color);
               var pixel = colorGradient.GetPixel(w, newH);
               var rounded = new Vector3Int(Mathf.FloorToInt(pixel.r * 1000f), Mathf.FloorToInt(pixel.g * 1000f), Mathf.FloorToInt(pixel.b * 1000f));
               if (!colorMap.ContainsKey(rounded)) colorMap.Add(rounded, new Vector2Int(w, newH));
            }
            for (int h = height / 2; h < height; h++)
            {
               var newH = (height - 1) - h;
               var color = Color.HSVToRGB((float)(w + 1) / (width - 1), 1, (1 - ((float)((h - (height * .5f)) * 2f)) / (height - 1)));
               colorGradient.SetPixel(w, newH, color);
               var pixel = colorGradient.GetPixel(w, newH);
               var rounded = new Vector3Int(Mathf.FloorToInt(pixel.r * 1000f), Mathf.FloorToInt(pixel.g * 1000f), Mathf.FloorToInt(pixel.b * 1000f));
               if (!colorMap.ContainsKey(rounded)) colorMap.Add(rounded, new Vector2Int(w, newH));
            }
         }
         colorGradient.filterMode = FilterMode.Point;
         colorGradient.Apply();

         ColorButton.image.sprite = Sprite.Create(colorGradient, new Rect(0, 0, width, height), new Vector2(0, 0));

         RoundIndex(isPrimary ? PrimaryIndex : SecondaryIndex);
      }

   }
}