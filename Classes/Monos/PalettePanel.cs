// � 2021, Minetso LLC. All Rights Reserved.



using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Minetso.Voxscape
{
   [ExecuteInEditMode]
   public class PalettePanel : MonoBehaviour
   {
      public GameObject Panel;
      public Texture2D ColorPalette;
      public BlockDesigner ObjectDesigner;
      public GameObject SelectionIndicator;



      private Dictionary<Vector2, Button> positionToButton = new Dictionary<Vector2, Button>();
      private Dictionary<Button, Vector2> buttonToPosition = new Dictionary<Button, Vector2>();

      private Vector2 currentIndex;
      /*
      private void Awake()
      {
         int count = Panel.transform.childCount;
         for (int i = 0; i < count; i++)
         {
            DestroyImmediate(Panel.transform.GetChild(0).gameObject);
         }
         positionToButton.Clear();
         buttonToPosition.Clear();
         //int y = 0; y < ColorPalette.height; y++
         //int x = 0; x < ColorPalette.width; x++
         Debug.Log($"Height is : {ColorPalette.height}");
         Debug.Log($"Width is : {ColorPalette.width}");
         for (int y = 0; y < ColorPalette.height; y++)
         {
            for (int x = 0; x < ColorPalette.width; x++)
            {
               var go = new GameObject();
               go.hideFlags = HideFlags.HideInHierarchy;
               go.transform.parent = Panel.transform;
               go.AddComponent<RectTransform>();
               var button = go.AddComponent<Button>();
               buttonToPosition.Add(button, new Vector2(x, y));
               positionToButton.Add(new Vector2(x, y), button);
               button.onClick.AddListener(
                  delegate { MoveSelectionIndication(button); });
               var image = go.AddComponent<Image>();
               button.targetGraphic = image;
               ColorBlock cb = button.colors;
               cb.normalColor = ColorPalette.GetPixel(x, y);
               button.colors = cb;
            }
         }
      }

      private void MoveSelectionIndication(Button button)
      {
         ObjectDesigner.BlockPrimaryColor = new Vector2Int(button.transform.GetSiblingIndex() % 8, Mathf.FloorToInt(button.transform.GetSiblingIndex() / 8));
         SelectionIndicator.transform.position = button.transform.position;
         currentIndex = buttonToPosition[button];
      }

      private void Update()
      {
         if (Input.GetKeyUp(Controls.UpArrow))
         {
            MoveSelectionIndication(positionToButton[new Vector2(currentIndex.x, (currentIndex.y + 1) % ColorPalette.height)]);
         }
         if (Input.GetKeyUp(Controls.DownArrow))
         {
            MoveSelectionIndication(positionToButton[new Vector2(currentIndex.x, currentIndex.y - 1 == -1 ? ColorPalette.height - 1 : currentIndex.y - 1)]);
         }
         if (Input.GetKeyUp(Controls.LeftArrow))
         {
            MoveSelectionIndication(positionToButton[new Vector2(currentIndex.x - 1 == -1 ? ColorPalette.width - 1 : currentIndex.x - 1, currentIndex.y)]);
         }
         if (Input.GetKeyUp(Controls.RightArrow))
         {
            MoveSelectionIndication(positionToButton[new Vector2((currentIndex.x + 1) % ColorPalette.width, currentIndex.y)]);
         }
      }

      */
   }
}