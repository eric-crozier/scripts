// � 2021, Minetso LLC. All Rights Reserved.

using Sirenix.OdinInspector;
using UnityEngine;

namespace Minetso.Voxscape
{
   [HideMonoScript]
   public class Controls : MonoBehaviour
   {
      #region Properties / Fields

      [FoldoutGroup("Control's Settings")] [ShowInInspector] [SerializeField] public static KeyCode Forward = KeyCode.W;
      [FoldoutGroup("Control's Settings")] [ShowInInspector] [SerializeField] public static KeyCode Left = KeyCode.A;
      [FoldoutGroup("Control's Settings")] [ShowInInspector] [SerializeField] public static KeyCode Right = KeyCode.D;
      [FoldoutGroup("Control's Settings")] [ShowInInspector] [SerializeField] public static KeyCode Backwards = KeyCode.S;
      [FoldoutGroup("Control's Settings")] [ShowInInspector] [SerializeField] public static KeyCode Up = KeyCode.Space;
      [FoldoutGroup("Control's Settings")] [ShowInInspector] [SerializeField] public static KeyCode Down = KeyCode.LeftControl;
      [FoldoutGroup("Control's Settings")] [ShowInInspector] [SerializeField] public static KeyCode LeftClick = KeyCode.Mouse0;
      [FoldoutGroup("Control's Settings")] [ShowInInspector] [SerializeField] public static KeyCode RightClick = KeyCode.Mouse1;
      [FoldoutGroup("Control's Settings")] [ShowInInspector] [SerializeField] public static KeyCode Rotation = KeyCode.Mouse2;
      [FoldoutGroup("Control's Settings")] [ShowInInspector] [SerializeField] public static KeyCode Boost = KeyCode.LeftShift;
      [FoldoutGroup("Control's Settings")] [ShowInInspector] [SerializeField] public static KeyCode Log = KeyCode.F5;
      [FoldoutGroup("Control's Settings")] [ShowInInspector] [SerializeField] public static KeyCode UpArrow = KeyCode.UpArrow;
      [FoldoutGroup("Control's Settings")] [ShowInInspector] [SerializeField] public static KeyCode DownArrow = KeyCode.DownArrow;
      [FoldoutGroup("Control's Settings")] [ShowInInspector] [SerializeField] public static KeyCode LeftArrow = KeyCode.LeftArrow;
      [FoldoutGroup("Control's Settings")] [ShowInInspector] [SerializeField] public static KeyCode RightArrow = KeyCode.RightArrow;
      [FoldoutGroup("Control's Settings")] [ShowInInspector] [SerializeField] public static KeyCode RotateTexture = KeyCode.R;
      [FoldoutGroup("Control's Settings")] [ShowInInspector] [SerializeField] public static KeyCode GrabFace = KeyCode.Mouse2;
      #endregion
   }
}