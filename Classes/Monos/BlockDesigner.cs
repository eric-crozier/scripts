// � 2021, Minetso LLC. All Rights Reserved.

using Sirenix.OdinInspector;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.UIElements;
using System.Linq;

namespace Minetso.Voxscape
{
   public class BlockDesigner : MonoBehaviour
   {
      [SerializeField]
      [ShowInInspector]
      public BlockData blockData;

      [SerializeField]
      private CreationInteraction creationInteraction;

      public static BlockDesigner blockDesigner;

      public Light Light;
      private static Light lightObj;

      public Texture2D BlockTexture;
      private Camera MainCamera;

      public Color BlockPrimaryColor;
      public Color BlockSecondaryColor;
      public Vector3Int TextureIndex = new Vector3Int(0, 0, 0);

      public UnityEngine.UI.Toggle TransparencyToggle;
      public UnityEngine.UI.Toggle EmissiveToggle;
      public UnityEngine.UI.Toggle LitToggle;
      [SerializeField] private UnityEngine.UI.Toggle paint;

      [ShowInInspector] [SerializeField] public float ClickDelay = .1f;
      [HideInInspector] [SerializeField] private static float timer;

      public VisualEffectAsset VisualEffectAsset;
      public static VisualEffect TempVfx;

      [SerializeField] private AudioClip createSound;
      [SerializeField] private AudioClip destroySound;
      [SerializeField] private AudioClip paintSound;
      [SerializeField] private AudioClip copySound;

      [SerializeField] private AudioSource audioScource;
      private (Vector3Int, int) GetBlockFace()
      {
         var point = MouseToBlockPos(true);
         var faceheading = Tools.V3FTOV3I(MouseToBlockPos(false) - point);

         if (faceheading == Vector3Int.up) { return (point, 0); }
         if (faceheading == Vector3Int.down) { return (point, 1); }
         if (faceheading == Vector3Int.back) { return (point, 2); }
         if (faceheading == Vector3Int.forward) { return (point, 3); }
         if (faceheading == Vector3Int.right) { return (point, 4); }
         if (faceheading == Vector3Int.left) { return (point, 5); }

         return (point, -1);
      }

      private Vector3Int MouseToBlockPos(bool returnBlockPos)
      {
         var screenPos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 1f);
         var mouseWorldPoint = MainCamera.ScreenToWorldPoint(screenPos);
         int fireDistance = (int)(200 + Vector3.Distance(mouseWorldPoint, transform.position));
         var heading = (mouseWorldPoint - (MainCamera.transform.position));
         var distance = heading.magnitude;
         var direction = (heading / distance) * .5f;

         Vector3Int validEmptyPosition = new Vector3Int(-1, -1, -1);

         for (int i = 0; i < fireDistance; i++)
         {
            var point = Tools.V3FTOV3I(mouseWorldPoint + (direction * i) + new Vector3(.5f, .5f, .5f));

            if (blockData.points.ContainsKey(point))
            {
               if (returnBlockPos)
               {
                  return point;
               }
               else
               {
                  return validEmptyPosition;
               }
            }
            else
            {
               var original = point;

               if (original.x < 0 || original.x > 99 || original.y < 0 || original.y > 99 || original.z < 0 || original.z > 99) continue;
               validEmptyPosition = point;
            }
         }
         return validEmptyPosition;
      }

      private void Awake()
      {
         blockData = new BlockData() { Connections = new bool[6] };
         lightObj = Light;
         blockDesigner = this;
         MainCamera = Camera.main;
         TempVfx = gameObject.AddComponent<VisualEffect>();
         TempVfx.visualEffectAsset = VisualEffectAsset;
         ParticleController.SetParticles(blockData.points, TempVfx);
      }

      private void Update()
      {
         timer += Time.deltaTime;
         if (!EventSystem.current.IsPointerOverGameObject())
         {
            if (Input.GetKey(Controls.GrabFace) && timer > ClickDelay)
            {
               var faceData = GetBlockFace();
               if (faceData.Item1 == new Vector3Int(-1, -1, -1) || faceData.Item2 == -1)
               {
                  return; ;
               }
               audioScource.PlayOneShot(copySound);
               var face = blockData.points[faceData.Item1].Faces[faceData.Item2];

               LitToggle.isOn = blockData.points[faceData.Item1].Lit;
               BlockPrimaryColor = face.BlockPrimaryColor;
               BlockSecondaryColor = face.BlockSecondaryColor;
               TextureIndex = face.TextureIndex;
               TransparencyToggle.isOn = face.Transparent;
               EmissiveToggle.isOn = face.Emissive;

               CreationInteraction.RefreshColors(BlockPrimaryColor, BlockSecondaryColor, TextureIndex);
               timer = 0;
            }

            if (paint.isOn)
            {
               if (Input.GetKey(Controls.LeftClick) && timer > ClickDelay)
               {
                  var faceData = GetBlockFace();
                  if (faceData.Item1 == new Vector3Int(-1, -1, -1) || faceData.Item2 == -1)
                  {
                     return; ;
                  }
                  blockData.points[faceData.Item1].Faces[faceData.Item2] = new PointData.Face()
                  {
                     BlockPrimaryColor = BlockPrimaryColor,
                     BlockSecondaryColor = BlockSecondaryColor,
                     TextureIndex = TextureIndex,
                     Transparent = TransparencyToggle.isOn,
                     Emissive = EmissiveToggle.isOn,
                  };
                  audioScource.PlayOneShot(paintSound);
                  ParticleController.SetParticles(blockData.points, TempVfx);
               }
               else if (Input.GetKey(Controls.RightClick) && timer > ClickDelay)
               {
                  var faceData = GetBlockFace();
                  if (faceData.Item1 == new Vector3Int(-1, -1, -1) || faceData.Item2 == -1)
                  {
                     return; ;
                  }

                  blockData.points[faceData.Item1].Faces[faceData.Item2] = new PointData.Face()
                  {
                     BlockPrimaryColor = BlockPrimaryColor,
                     BlockSecondaryColor = BlockSecondaryColor,
                     TextureIndex = new Vector3Int(0, 0, 0),
                     Transparent = false,
                     Emissive = false,
                  };
                  audioScource.PlayOneShot(paintSound);
                  ParticleController.SetParticles(blockData.points, TempVfx);
               }
            }
            else
            {
               if (Input.GetKey(Controls.LeftClick) && timer > ClickDelay)
               {
                  timer = 0;

                  Vector3Int pos = MouseToBlockPos(false);
                  if (pos != new Vector3Int(-1, -1, -1))
                  {
                     if (!blockData.points.ContainsKey(pos))
                     {
                        blockData.points.Add(pos, new PointData()
                        {
                           Position = pos,
                           Lit = LitToggle.isOn
                        });

                        for (int i = 0; i < 6; i++)
                        {
                           blockData.points[pos].Faces.Add(new PointData.Face()
                           {
                              BlockPrimaryColor = BlockPrimaryColor,
                              BlockSecondaryColor = BlockSecondaryColor,
                              TextureIndex = TextureIndex,
                              Transparent = TransparencyToggle.isOn,
                              Emissive = EmissiveToggle.isOn,
                           });
                        }
                        audioScource.PlayOneShot(createSound);
                        ParticleController.SetParticles(blockData.points, TempVfx);
                     }
                  }

               }
               else if (Input.GetKey(Controls.RightClick) && timer > ClickDelay)
               {
                  timer = 0;

                  var pos = MouseToBlockPos(true);
                  if (pos != new Vector3Int(-1, -1, -1))
                  {
                     if (blockData.points.ContainsKey(pos))
                     {
                        var point = blockData.points[pos];
                        if (point.Lit)
                        {
                           Destroy(point.Light);
                        }
                        audioScource.PlayOneShot(destroySound);
                        blockData.points.Remove(pos);
                        ParticleController.SetParticles(blockData.points, TempVfx);
                     }
                  }
               }
            }
         }
      }

      public static void SpawnLight(Vector3Int point, Color color, Dictionary<Vector3Int, PointData> points)
      {
         var light = Instantiate(lightObj);
         points[point].Light = light.gameObject;
         light.gameObject.isStatic = true;
         // light.lightmapBakeType = LightmapBakeType.Baked;
         light.gameObject.transform.parent = blockDesigner.transform;
         light.intensity = 3000000;
         light.color = color;
         light.gameObject.transform.position = point;
         light.gameObject.SetActive(true);
      }

      [Button]
      public void SaveBlock()
      {
         // var listOfPoints = points.Values.ToArray();
         blockData.pointData = blockData.points.Values.ToList();
         var saveData = JsonUtility.ToJson(blockData);
         /*
         var saveData = string.Empty;
         for(int i = 0; i < listOfPoints.Count(); i++)
         {
            saveData += $"{JsonUtility.ToJson(listOfPoints[i])}|";
         }
         */
         Debug.Log(saveData);

         savedData = saveData;
        // LoadBlock(saveData);
      }

      private string savedData;

      [Button]
      public void LoadBlock()
      {

         blockData = new BlockData(JsonUtility.FromJson(savedData, typeof(BlockData)) as BlockData);
         creationInteraction.BlockNameInputField.text = blockData.Name;
         creationInteraction.BlockMainTagInputField.text = blockData.MainTag;
         creationInteraction.BlockSubTagInputField.text = blockData.SubTag;

         /*
         foreach (var line in savedData.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries))
         {
            PointData pd = JsonUtility.FromJson(line, typeof(PointData)) as PointData;

            points.Add(pd.Position, pd);
         }
         */
         ParticleController.SetParticles(blockData.points, TempVfx);
      }
   }
}