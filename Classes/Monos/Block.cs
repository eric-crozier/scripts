// � 2021, Minetso LLC. All Rights Reserved.

using UnityEngine;

namespace Minetso.Voxscape
{
   public class Block : MonoBehaviour
   {
      public Vector3Int Position;
      public Vector3Int Rotation;
      public BlockData BlockData;
   }
}