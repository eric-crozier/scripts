// � 2021, Minetso LLC. All Rights Reserved.

using UnityEngine;
using UnityEngine.UI;

namespace Minetso.Voxscape
{
   public class SliderValueChange : MonoBehaviour
   {
      [SerializeField] private Slider slider;
      [SerializeField] private Text text;
      [SerializeField] private Image selectedimage;

      private void Start()
      {
         slider.onValueChanged.AddListener(delegate { UpdateSlider(); });
         selectedimage.color = Color.HSVToRGB(slider.value / (slider.maxValue + 1), 1, 1);
      }

      void UpdateSlider()
      {
         text.text = slider.value.ToString();
         selectedimage.color = Color.HSVToRGB(slider.value / (slider.maxValue + 1), 1, 1);
      }
   }
}

