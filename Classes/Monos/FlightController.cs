// � 2021, Minetso LLC. All Rights Reserved.

using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Minetso.Voxscape
{
   [HideMonoScript]
   public class FlightController : Cycle
   {
      #region Properties / Fields

      [FoldoutGroup("Flight Controller Settings")] [ShowInInspector] [SerializeField] public Camera MainCamera;
      [FoldoutGroup("Flight Controller Settings")] [ShowInInspector] [SerializeField] public Transform MainTransform;
      [FoldoutGroup("Flight Controller Settings")] [ShowInInspector] [SerializeField] public float MovementSpeed = 1;
      [FoldoutGroup("Flight Controller Settings")] [ShowInInspector] [SerializeField] public float MouseSensitivity = 1;
      [FoldoutGroup("Flight Controller Settings")] [HideInInspector] [SerializeField] private int boostMultiplier = 10;

      #endregion

      #region Methods

      protected override void Update()
      {
         base.Update();
         if (!EventSystem.current.IsPointerOverGameObject())
         {
            Movement();
            Rotation();
         }
      }

      private void Movement()
      {
         if (Input.GetKey(Controls.Boost))
         {
            boostMultiplier = 10;
         }
         else
         {
            boostMultiplier = 1;
         }

         if (Input.GetKey(Controls.Forward))
         {
            MainTransform.position += MainTransform.forward * MovementSpeed * boostMultiplier;
         }
         if (Input.GetKey(Controls.Backwards))
         {
            MainTransform.position += -MainTransform.forward * MovementSpeed * boostMultiplier;
         }
         if (Input.GetKey(Controls.Right))
         {
            MainTransform.position += MainTransform.right * MovementSpeed * boostMultiplier;
         }
         if (Input.GetKey(Controls.Left))
         {
            MainTransform.position += -MainTransform.right * MovementSpeed * boostMultiplier;
         }
         if (Input.GetKey(Controls.Up))
         {
            MainTransform.position += MainTransform.up * MovementSpeed * boostMultiplier;
         }
         if (Input.GetKey(Controls.Down))
         {
            MainTransform.position += -MainTransform.up * MovementSpeed * boostMultiplier;
         }
      }

      private void Rotation()
      {
         if (!Input.GetKey(Controls.Rotation)) return;

         float mouseX = Input.GetAxis("Mouse X") * MouseSensitivity * Time.deltaTime;
         float mouseY = Input.GetAxis("Mouse Y") * MouseSensitivity * Time.deltaTime;

         MainTransform.eulerAngles = new Vector3(MainTransform.eulerAngles.x - mouseY, MainTransform.eulerAngles.y  + mouseX, 0f);
      }

      #endregion
   }
}