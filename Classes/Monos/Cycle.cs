// � 2021, Minetso LLC. All Rights Reserved.

using UnityEngine;
using Sirenix.OdinInspector;

namespace Minetso.Voxscape
{
   [HideMonoScript]
   public class Cycle : SerializedMonoBehaviour
   {
      #region Properties / Fields

      [FoldoutGroup("Cycle Settings")] [HideInInspector] [SerializeField] private float elapsed;
      [FoldoutGroup("Cycle Settings")] [ShowInInspector] [SerializeField] public int Fps = 4;

      #endregion

      #region Methods

      protected virtual void Update()
      {
         if (Fps <= 0) return;

         elapsed += (Mathf.RoundToInt(Time.deltaTime * 1000f)) * 0.001f;
         if (elapsed >= (1f / Fps))
         {
            elapsed %= (1f / Fps);
            CustomUpdate();
         }
      }

      protected virtual void CustomUpdate() { }

      #endregion
   }
}