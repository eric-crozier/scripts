// � 2021, Minetso LLC. All Rights Reserved.

using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Minetso.Voxscape
{
   public class Logger : MonoBehaviour
   {
      [SerializeField] private GameObject scrollPanel;
      [SerializeField] private Text logHistory;
      [SerializeField] private Text currentLog;

      #region Methods

      private void Awake()
      {
         Info.Logger = this;
      }

      private void Update()
      {
         if (Input.GetKeyUp(Controls.Log))
         {
            scrollPanel.SetActive(!scrollPanel.activeSelf);
         }
      }

      public void UpdateLog(string time, string message)
      {
         logHistory.text += $"{time} : {message}\n";
         currentLog.text = message;
         StopAllCoroutines();
         StartCoroutine(Clear());
      }

      private IEnumerator Clear()
      {
         yield return new WaitForSeconds(2);
         currentLog.text = string.Empty;
      }

      #endregion
   }
}